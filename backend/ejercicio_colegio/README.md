# Ejercicio Colegio

![modelo](modelo.PNG)

Eres parte de un proyecto para una institución educativa. Te entregan el modelo entidad relación y te piden crear un microservicio.

## Actividades a desarrollar

1. Crear proyecto de Spring
2. Crear paquete entities
3. Crear entidades a partir del diagrama
4. Crear un repositorio para cada entidad
5. Usar pruebas unitarias para crear y buscar entidades.

## Actividades extra

Hacer que los repositorios puedan consultar:​

- Un estudiante por nombres y apellidos​

- Imprimir la lista de profesores del estudiante consultado​

- Consultar los cursos según el código tal que contengan la letra O