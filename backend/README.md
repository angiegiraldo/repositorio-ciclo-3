# Backend en Java - Spring Boot

Bienvenidos tripulantes, en esta carpeta encontrarán las demostraciones hechas en las sesiones.

## ¿Qué es Spring Boot?

SpringBoot es una herramienta que permite el desarrollo rápido y seguro de aplicaciones de backend.

## Guía paso a paso para el desarrollo en SpringBoot

A continuación veremos un ejemplo de una implementación de software basado en Spring usando una guía paso a paso.

![modelo](modelo_db.png)

A partir del modelo de base de datos mostrado se crean todos los componentes de un proyecto en Spring empezando desde las entidades hasta exponer el servicio.

### Creación del proyecto

Los proyectos se generan en Spring Init. Abrimos el [proyecto predefinido en la plataforma dando clic aquí](https://start.spring.io/#!type=maven-project&language=java&platformVersion=2.7.3&packaging=jar&jvmVersion=11&groupId=com.misiontic&artifactId=demo&name=demo&description=Plantilla%20inicial%20del%20proyecto%20implementado%20en%20Mision%20TIC%20UIS&packageName=com.misiontic.demo&dependencies=devtools,lombok,configuration-processor,web,data-jpa,mysql). Descargamos el proyecto dando clic en *Generate* y extraemos el proyecto comprimido. Esa carpeta extraida ya es nuestro proyecto de *Spring*, usamos *Visual Studio Code* para abrir el proyecto. La estructura del proyecto se muestra en la siguiente imagen.

![Estructura Proyecto](estructura_java_vscode.PNG)

### Creación de paquetes, clases o interfaces

1. El proyecto de Spring se separa en diferentes paquetes, para crear un paquete se hace clic en el paquete principal que se encuentra en src/main/java.

![Crear Paquete](crear_paquete.PNG)

Como se muestra en la imagen colocamos el nombre del paquete despues de todo el paquete principal, por ejemplo: com.misiontic.demo.*entities*

2. Las clases se crean en los paquetes, damos clic en el simbolo + para crear una clase, basta con colocar solo el nombre de la clase, ejemplo: *Usuario*.

### Entidades

Una entidad en *Spring* es una clase que representa una tabla en la base de datos. Para definir entidades se realiza el siguiente procedimiento:

1. Se crea una clase que represente una tabla en la base de datos, según el modelo de la base de datos tenemos:

```
public class Rol{

}
```

2. Agregamos los atributos necesarios de la clase (los atributos representan columnas en este caso)

```
public class Rol{
    private int id;
    private int nombres;
}
```

3. Ahora agregamos las anotaciones para Spring.`@Entity` anota una clase para marcarla como entidad (una entidad representa una tabla). `@Id` anota el atributo que representa la llave primaria. `@GeneratedValue(strategy = GenerationType.IDENTITY)` anota el atributo de llave primaria como autoincrementable. `@Column` anota el atributo para representarlo como una columna.

```
@Entity
public class Rol{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)​
    private int id;
    @Column
    private int nombres;
}
```

### Relaciones de entidades

Del paso anterior vimos que, para la implementación de entidades se crean primero las clases que no representan tablas con llaves foraneas. Entonces, siguiendo el modelo E-R propuesto implementamos las otras clases.

4. Hacemos el paso 1 y 2 para crear la otra clase, en este ejemplo la clase quedaria asi:

```
public class Usuario{
    private int id;
    private String nombres;
    private String apellidos;
    private String cedula;
    private int edad;
    private boolean estaActivo;
    private LocalDateTime fechaCreacionUsuario;
    private Rol rol;
}
```

5. Siguiendo el paso 3 agregamos las anotaciones. Como muchos Usuarios pueden tener un rol entonces usamos la anotación `@ManyToOne`, adicionalmente agregamos la anotación `@JoinColumn(nullable=false)` para indicar que se va crear una columna que tenga la llave foranea y su campo sea obligatorio.

```
@Entity
public class Usuario{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)​
    private int id;
    @Column
    private String nombres;
    @Column
    private String apellidos;
    @Column
    private String cedula;
    @Column
    private int edad;
    @Column
    private boolean estaActivo;
    @Column
    private LocalDateTime fechaCreacionUsuario;
    @ManyToOne
    @JoinColumn(nullable=false)
    private Rol rol;
}
```

Si es una relación 1 a 1 entonces en vez de colocar `@ManyToOne` se asigna la relación `@OneToOne`. Si es una relación de muchos a muchos se usa una colección como tipo (ejemplo: `List<Usuario>`) en ambas clases y se anota unicamente con `@ManyToMany`.

## Repositorios

6. Se crea el repositorio como una interfaz, con el nombre de la entidad asociada y la palabra Repo

```
public interfaz UsuarioRepo{
}
```

```
public interfaz RolRepo{
}
```

7. Se implementa herencia usando la interfaz `CrudRepository<S,ID>`

```
public interfaz UsuarioRepo extends CrudRepository<S,ID> { 
}
```

```
public interfaz RolRepo CrudRepository<S,ID> {
}
```

8. Se reemplaza S por la clase a la que queremos asignar métodos *CRUD (CREATE, READ, UPDATE, DELETE)* y se reemplaza ID por la clase según el tipo del atributo que tiene la anotación `@Id`

```
public interfaz UsuarioRepo extends CrudRepository<Usuario,Integer> { 
}
```

```
public interfaz RolRepo CrudRepository<Rol,Integer> {
}
```

Ahora, ya podemos usar los repositorios para usar métodos CRUD sobre nuestras entidades.

## Controladores REST

Los controladores REST son clases que contienen los llamados *EndPoint API* para exponer servicios a diferentes aplicaciones. Más información en [Spring.io](https://spring.io/guides/tutorials/rest/). En los controladores reposa la lógica que se realiza con los datos que se reciben del cliente y la base de datos. Para implementar controladores se crean clases en el paquete *controllers*. El paso a paso es como sigue,

1. Crear una clase controlador y anotarla usando `@RestController`

```
@RestController
public class Controlador{

}
```

2. Como el controlador necesita los métodos CRUD, se realiza la inyección de dependencias, esto es, usar los repositorios creados en los atributos e indicar por medio de la anotación `@Autowired` que Spring cree un objeto con los correspondientes métodos.

```
@RestController
public class Controlador{

    @Autowired
    private RolRepo rolRepo;
    @Autowired
    private UsuarioRepo usuarioRepo;

}
```

3. Según los requerimientos funcionales o historias de usuario, se crean métodos que resuelvan los requerimientos. Por ejemplo, tenemos una historia de usuario como requerimiento: **Como** Administrador **quiero** crear roles **para** poder asignar diferentes permisos a los usuarios del sistema. Según la historia de usuario deducimos que va haber un metodo CRUD (el cual es CREATE) para la entidad rol.

```
@RestController
public class Controlador{

    @Autowired
    private RolRepo rolRepo;
    @Autowired
    private UsuarioRepo usuarioRepo;

    public void crearRol(){
        
    }

}
```