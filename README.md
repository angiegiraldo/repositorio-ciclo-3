# Bases de datos relacionales usando MySQL

## Intro
Las bases de datos compuestas por tablas en su gran mayoria de casos se encuentran relacionadas. Esto es, que una columna de una tabla hace referencia a otra.

![ejemplo relacion](./ejemplo.png)

Si nos fijamos en el ejemplo, todas las tablas deben tener una columna caracterizada como llave primaria (PK), es decir, los registros en esa columna son únicos. Sin embargo, hablamos de llaves foraneas (FK) cuando una columna de la tabla hace referencia a otra tabla (exactamente apunta a la PK de la otra tabla). En este caso, por medio de las FK se relacionan diferentes tablas o según el modelo del negocio se relacionan entidades. Existe otra forma de modelar datos, usando el modelo entidad-relación con [notación de Chen](https://ewebik.com/base-de-datos/modelo-entidad-relacion). Esta notación se acerca un poco más al problema representando relaciones entre entidades con verbos.

## Ejemplo de modelado con notación Chen

1. Según el [ejemplo de W3School](https://www.w3schools.com/mysql/mysql_rdbms.asp), realiza el modelo con ayuda del formador usando [notación de Chen](https://ewebik.com/base-de-datos/modelo-entidad-relacion). (en las próximas sesiones se verá el paso a paso realizado acá). Hacer el modelo entidad relacion en papel según el ejemplo de base de datos de W3School.
