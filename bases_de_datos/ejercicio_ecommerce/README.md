# Ejercicio de diseño de bases de datos - Ecommerce

Deseas implementar un proyecto generico para e-commerce con el fin de ofrecer a empresas  la implementación de su propio sitio de comercio web. Por esta razón, usas Excel para representar todas las tablas a usar y sus diferentes relaciones.

## Actividades a desarrollar

### Diseño de base de datos en papel

1. Usando el modelo entidad-relación con [notación de Chen](https://ewebik.com/base-de-datos/modelo-entidad-relacion) representa la base de datos a partir del Excel.


### Diseño en MySQL Workbench

1. Usa los diseños anteriores para crear el modelo de la base de datos en *MySQL Workbench*.

### Implementación

1. Usa Forward Engineering para migrar del modelo al servidor de *MySQL*

### Migración de datos - de Excel a MySQL

1. Sigue las instrucciones del formador para migrar datos del Excel a la base de datos implementada en MySQL.

## Referencia

- Diseño extraído de [fabric.inc](https://fabric.inc/blog/ecommerce-database-design-example/)